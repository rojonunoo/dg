<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::get('/','PagesController@home')->name('home');
Route::get('about-us','PagesController@about_us')->name('about-us');
Route::get('our-business','PagesController@our_business')->name('our-business');
Route::get('mission','PagesController@mission')->name('mission');
Route::get('faq','PagesController@faq')->name('faq');
Route::get('contact-us','PagesController@contact_us')->name('contact-us');