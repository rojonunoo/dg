@extends('layouts.app')
@section('title')
    Mission
@endsection

@section('content')
    <!-- Page title -->
    <div class="page-title parallax-style parallax1">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-title-heading">
                        <h2>Our Mission</h2>
                    </div><!-- /.page-title-heading -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-title -->

    <div class="page-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="flat-wrapper">
                    <div class="breadcrumbs">
                        <h2 class="trail-browse">You are here:</h2>
                        <ul class="trail-items">
                            <li class="trail-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="trail-item"><a href=""> Company</a></li>
                            <li class="tail-item active">OUR MISSION</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.flat-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-breadcrumbs -->

    <div class="flat-row">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tracking-form-div">
                        <h3 class="flat-title-section style mag-top0px">MISSION</h3>
                        <p>To create an ultimate professional service in trading markets</p>

                        <p>To provide each client with a personal approach and with with premium offer Brands</p>

                        <p>To bring the world’s top leading retail brands to your doorstep</p>

                        <p>To develop and to increase Brand Portfolio</p>

                        <p>To be considered the benchmark of the trading industry because of us the sky is the limit!</p>
                    </div><!-- /.tracking-form-div -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-row -->
@endsection
