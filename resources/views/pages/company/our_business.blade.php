@extends('layouts.app')
@section('title')
    Contact Us
@endsection

@section('content')
    <div class="page-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="flat-wrapper">
                    <div class="breadcrumbs">
                        <h2 class="trail-browse">You are here:</h2>
                        <ul class="trail-items">
                            <li class="trail-item"><a href="index-v2.html">Home</a></li>
                            <li>OU</li>
                        </ul>
                    </div><!-- /.breadcrumbs -->
                </div><!-- /.flat-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.page-breadcrumbs -->

    <div class="flat-row">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="tracking-form-div">
                        <h3 class="flat-title-section style mag-top0px">Track an Order</h3>
                        <p>Enter the order number you would like to track in the form below. Here are the demo order numbers : 001, 002, 003, 004</p>
                        <form class="tracking-form">
                            <div class="pure-control-group abs">
                                <label>Order Number: </label>
                                <input type="text" placeholder="Order Number...">
                            </div>
                            <div class="pure-control-group">
                                <input type="submit" value="Track">
                            </div>
                        </form><!-- /.tracking-form -->
                    </div><!-- /.tracking-form-div -->
                </div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-row -->
    @endsection