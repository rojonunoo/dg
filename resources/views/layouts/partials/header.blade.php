<div class="site-header header-v2">
    <div class="flat-top">
        <div class="container">
            <div class="row">
                <div class="flat-wrapper">
                    <div class="custom-info">
                        <span>Have any questions?</span>
                        <span><i class="fa fa-reply"></i>info@directgroups.org</span>
                        <span><i class="fa fa-map-marker"></i>Taojin Garden Block, B2023,Yuexiu District</span>
                        <span><i class="fa fa-phone"></i>+ 012 222 989899</span>
                    </div>

                    <div class="social-links">
                        <a href="#">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-facebook-official"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-google-plus"></i>
                        </a>
                        <a href="#">
                            <i class="fa fa-pinterest"></i>
                        </a>
                    </div>
                </div><!-- /.flat-wrapper -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-top -->

    <header id="header" class="header">
    <div class="header-wrap">
        <div id="logo" class="logo">
            <a href="index.html">
                <img src="images/logo.png" alt="images">
            </a>
        </div><!-- /.logo -->
        <div class="btn-menu">
            <span></span>
        </div><!-- //mobile menu button -->

        @include('layouts.partials.nav')
        </div><!-- /.nav-wrap -->
    </div><!-- /.header-wrap -->
</header><!-- /.header -->
</div><!-- /.site-header -->