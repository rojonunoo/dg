<div class="nav-wrap">
    <nav id="mainnav" class="mainnav">
        <div class="menu-extra">

        </div><!-- /.menu-extra -->
        <ul class="menu">
            <li class="home">
                <a href="{{route('home')}}">Home</a>
            </li>
            <li>
                <a href="#">Company</a>
                <ul class="submenu">
                    <li><a href="{{route('about-us')}}">About Us</a></li>
                    <li><a href="{{route('our-business')}}">Our Business</a></li>
                    <li><a href="{{route('mission')}}">Mission</a></li>
                    <li><a href="{{route('faq')}}">FAQ</a></li>
                </ul><!-- /.submenu -->
            </li>
            <li><a href="#">Services</a>
                <ul class="submenu">
                    <li><a href="#">Global Sourcing and Trading</a></li>
                    <li><a href="#">Logistics solutions</a></li>
                    <li><a href="#">Sourcing</a></li>
                    <li><a href="#">Business Advisory</a></li>
                    <li><a href="#">Warehousing and storage</a></li>
                </ul><!-- /.submenu -->
            </li>
            <!-- <li><a href="portfolio.html">Gallery</a>
                <ul class="submenu">
                    <li><a href="portfolio-v1.html">Layout 01</a></li>
                    <li><a href="portfolio-grid.html">Layout 02</a></li>
                    <li><a href="portfolio-masonry.html">Layout 03</a></li>
                </ul><! /.submenu -->
            <!-- </li>                                               -->
            <li><a href="blog.html">News</a>
                <ul class="submenu">
                    <li><a href="blog.html">Default layout</a></li>
                    <li><a href="blog-grid.html">Grid layout</a></li>
                </ul><!-- /.submenu -->
            </li>
            <!-- <li><a href="#">Pages</a>
                <ul class="submenu">
                    <li><a href="one-page.html">One page</a></li>
                    <li><a href="track-an-order.html">Track your shipment</a></li>
                    <li><a href="quote.html">Request a quote</a></li>
                    <li><a href="shortcode.html">Shortcode</a></li>
                    <li><a href="typography.html">Typography</a></li>
                    <li><a href="404.html">404 page</a></li>
                    <li><a href="search.html">Search results</a></li>
                </ul><! /.submenu -->
            </li>
            <!-- <li><a href="shop.html">Shop</a></li> -->
            <li><a href="contact.html">Contact</a></li>
        </ul><!-- /.menu -->
    </nav><!-- /.mainnav -->