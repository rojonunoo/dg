{{--<!DOCTYPE html>--}}
{{--<!--[if IE 8 ]><html class="ie" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->--}}
{{--<!--[if (gte IE 9)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->--}}
{{--<head>--}}
    {{--<!-- Basic Page Needs -->--}}
    {{--<meta charset="utf-8">--}}
    {{--<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->--}}
    {{--<title> @yield('title') </title>--}}

    {{--<meta name="author" content="directgroup.org">--}}

    {{--<!-- Mobile Specific Metas -->--}}
    {{--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">--}}

    {{--<!-- Bootstrap  -->--}}
    {{--<link rel="stylesheet" type="text/css" href="stylesheets/bootstrap.css" >--}}

    {{--<!-- Theme Style -->--}}
    {{--<link rel="stylesheet" type="text/css" href="stylesheets/style.css">--}}

    {{--<!-- Responsive -->--}}
    {{--<link rel="stylesheet" type="text/css" href="stylesheets/responsive.css">--}}

    {{--<!-- Colors -->--}}
    {{--<link rel="stylesheet" type="text/css" href="stylesheets/colors/color1.css" id="colors">--}}

    {{--<!-- Animation Style -->--}}
    {{--<link rel="stylesheet" type="text/css" href="stylesheets/animate.css">--}}

    {{--<!-- Favicon and touch icons  -->--}}
    {{--<link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">--}}
    {{--<link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">--}}
    {{--<link href="icon/favicon.png" rel="shortcut icon">--}}

    {{--<!--[if lt IE 9]>--}}
    {{--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>--}}
    {{--<![endif]-->--}}
{{--</head>--}}
{{--<body class="header-sticky ">--}}
{{--<div class="loading-overlay">--}}
{{--</div>--}}

{{--<!-- Boxed -->--}}
{{--<div class="boxed">--}}
       {{--@include('layouts.partials.header')--}}

    {{--<!-- Slider -->--}}
    {{--<div class="tp-banner-container">--}}
        {{--<div class="tp-banner" >--}}
            {{--<ul>--}}
                {{--<li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">--}}
                    {{--<img src="images/slides/1.jpg" alt="slider-image" />--}}
                    {{--<div class="tp-caption sfl title-slide center" data-x="40" data-y="100" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">--}}
                        {{--Standard sea freight<br>services--}}
                    {{--</div>--}}
                    {{--<div class="tp-caption sfr desc-slide center" data-x="40" data-y="240" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">--}}
                        {{--If low costs matter for your shipment, try our sea freight<br>services.--}}
                    {{--</div>--}}
                    {{--<div class="tp-caption sfl flat-button-slider bg-button-slider-32bfc0" data-x="40" data-y="370" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Read more</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></div>--}}

                    {{--<div class="tp-caption sfr flat-button-slider" data-x="225" data-y="370" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Contact us</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></div>--}}
                {{--</li>--}}

                {{--<li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">--}}
                    {{--<img src="images/slides/2.jpg" alt="slider-image" />--}}
                    {{--<div class="tp-caption sfl title-slide center" data-x="40" data-y="100" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">--}}
                        {{--International road<br>transport--}}
                    {{--</div>--}}
                    {{--<div class="tp-caption sfr desc-slide center" data-x="40" data-y="240" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">--}}
                        {{--The road transport industry is the backbone of strong<br>economies and dynamic societies.--}}
                    {{--</div>--}}
                    {{--<div class="tp-caption sfl flat-button-slider bg-button-slider-32bfc0" data-x="40" data-y="370" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Read more</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></div>--}}

                    {{--<div class="tp-caption sfr flat-button-slider" data-x="225" data-y="370" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Contact us</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></div>--}}
                {{--</li>--}}

                {{--<li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">--}}
                    {{--<img src="images/slides/3.jpg" alt="slider-image" />--}}
                    {{--<div class="tp-caption sfl title-slide center" data-x="40" data-y="100" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">--}}
                        {{--Warehousing and<br>storage--}}
                    {{--</div>--}}
                    {{--<div class="tp-caption sfr desc-slide center" data-x="40" data-y="240" data-speed="1000" data-start="1500" data-easing="Power3.easeInOut">--}}
                        {{--Warehouse services can be offered as a single service or<br>combined with transportation.--}}
                    {{--</div>--}}
                    {{--<div class="tp-caption sfl flat-button-slider bg-button-slider-32bfc0" data-x="40" data-y="370" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Read more</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></div>--}}

                    {{--<div class="tp-caption sfr flat-button-slider" data-x="225" data-y="370" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Contact us</a>&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></div>--}}
                {{--</li>--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<!-- Promobox -->--}}
    {{--<div class="flat-row bg-scheme pad-top0px pad-bottom0px">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="promobox style1 style2 clearfix">--}}
                        {{--<h5 class="promobox-title">We are honored to be a leading and reliable partner in the field of multimodal transport in US</h5>--}}
                        {{--<a class="button black sm" href="contact.html">contact us<i class="fa fa-chevron-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-12 -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<!-- Flat imagebox -->--}}
    {{--<div class="flat-row parallax-style parallax1">--}}
        {{--<div class="overlay bg-scheme1"></div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="flat-wrapper">--}}
                    {{--<div class="flat-imagebox clearfix">--}}
                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="imagebox">--}}
                                {{--<div class="box-wrapper">--}}
                                    {{--<div class="box-image">--}}
                                        {{--<img src="images/imagebox/1.jpg" alt="images">--}}
                                    {{--</div>--}}
                                    {{--<div class="box-header">--}}
                                        {{--<h5 class="box-title">--}}
                                            {{--<a href="services-detail.html">Why choose canava transport ?</a>--}}
                                        {{--</h5>--}}
                                    {{--</div>--}}
                                    {{--<div class="box-content">--}}
                                        {{--<div class="box-desc">At Canava Transport, we know time is of the essence. We have used our legacy Truckload service in the Mid-Atlantic and Midwest regions to shape what our company is today.</div>--}}
                                        {{--<div class="box-button">--}}
                                            {{--<a class="button bg-scheme3" href="about.html">read more <i class="fa fa-chevron-right"></i></a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div><!-- /.imagebox -->--}}
                        {{--</div><!-- /.item-three-column -->--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="imagebox">--}}
                                {{--<div class="box-wrapper">--}}
                                    {{--<div class="box-image">--}}
                                        {{--<img src="images/imagebox/2.jpg" alt="images">--}}
                                    {{--</div>--}}
                                    {{--<div class="box-header">--}}
                                        {{--<h5 class="box-title">--}}
                                            {{--<a href="services-detail-v1.html">Are you optimising your warehouse space ?</a>--}}
                                        {{--</h5>--}}
                                    {{--</div>--}}
                                    {{--<div class="box-content">--}}
                                        {{--<div class="box-desc">Warehousing, Storage and 3PL services offered by the Canava group have become an integral part of our client’s requirements. As our clients continued to demand increased savings and efficiencies in their businesses.</div>--}}
                                        {{--<div class="box-button">--}}
                                            {{--<a class="button bg-scheme3" href="about.html">read more <i class="fa fa-chevron-right"></i></a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div><!-- /.imagebox -->--}}
                        {{--</div><!-- /.item-three-column -->--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="imagebox">--}}
                                {{--<div class="box-wrapper">--}}
                                    {{--<div class="box-image">--}}
                                        {{--<img src="images/imagebox/3.jpg" alt="images">--}}
                                    {{--</div>--}}
                                    {{--<div class="box-header">--}}
                                        {{--<h5 class="box-title">--}}
                                            {{--<a href="services-detail-v2.html">The gallery of canava transport</a>--}}
                                        {{--</h5>--}}
                                    {{--</div>--}}
                                    {{--<div class="box-content">--}}
                                        {{--<div class="box-desc">Some images highlighting our warehouse, transport, Cargo and logistics expertise.</div>--}}
                                        {{--<div class="box-button">--}}
                                            {{--<a class="button bg-scheme3" href="about.html">read more <i class="fa fa-chevron-right"></i></a>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div><!-- /.imagebox -->--}}
                        {{--</div><!-- /.item-three-column -->--}}
                    {{--</div><!-- /.flat-imagebox -->--}}
                {{--</div><!-- /.flat-wrapper -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<!-- Flat iconbox style -->--}}
    {{--<div class="flat-row pad-top60px pad-bottom10px">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<h2 class="flat-title-section style">What we <span class="scheme">offer.</span></h2>--}}
                {{--</div><!-- /.col-md-12 -->--}}
            {{--</div><!-- /.row -->--}}
            {{--<div class="flat-divider d40px"></div>--}}
            {{--<div class="row">--}}
                {{--<div class="flat-wrapper">--}}
                    {{--<div class="flat-iconbox-style clearfix">--}}
                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="iconbox style1">--}}
                                {{--<div class="box-header">--}}
                                    {{--<div class="box-icon"><i class="fa fa-dropbox"></i></div>--}}
                                    {{--<h5 class="box-title">Packaged goods transport</h5>--}}
                                {{--</div>--}}
                                {{--<div class="box-content">--}}
                                    {{--Focuses on the packaging requirements of goods in transit, in particular for items traveling overland by road or rail.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.item-three-column -->--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="iconbox style1">--}}
                                {{--<div class="box-header">--}}
                                    {{--<div class="box-icon"><i class="fa fa-truck"></i></div>--}}
                                    {{--<h5 class="box-title">Multimodal transport</h5>--}}
                                {{--</div>--}}
                                {{--<div class="box-content">--}}
                                    {{--Combined rail road transport is particularly well suited to the shipping of hazardous goods since it reduces risk.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.item-three-column -->--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="iconbox style1">--}}
                                {{--<div class="box-header">--}}
                                    {{--<div class="box-icon"><i class="fa fa-cube"></i></div>--}}
                                    {{--<h5 class="box-title">Warehousing and storage</h5>--}}
                                {{--</div>--}}
                                {{--<div class="box-content">--}}
                                    {{--Canava is able to offer heated or unheated warehouse solutions both for short-term and for long-term storage.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.item-three-column -->--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="row">--}}
                {{--<div class="flat-wrapper">--}}
                    {{--<div class="flat-iconbox-style clearfix">--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="iconbox style1">--}}
                                {{--<div class="box-header">--}}
                                    {{--<div class="box-icon"><i class="fa fa-mail-forward"></i></div>--}}
                                    {{--<h5 class="box-title">Forwarding services</h5>--}}
                                {{--</div>--}}
                                {{--<div class="box-content">--}}
                                    {{--With our extensive network, we will find a competitive and efficient solution to your next assignment.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.item-three-column -->--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="iconbox style1">--}}
                                {{--<div class="box-header">--}}
                                    {{--<div class="box-icon"><i class="fa fa-plane"></i></div>--}}
                                    {{--<h5 class="box-title">Sea and air freight</h5>--}}
                                {{--</div>--}}
                                {{--<div class="box-content">--}}
                                    {{--By using a combination of sea and air freight, you bring added flexibility to your supply chain.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.item-three-column -->--}}

                        {{--<div class="flat-item item-three-column">--}}
                            {{--<div class="iconbox style1">--}}
                                {{--<div class="box-header">--}}
                                    {{--<div class="box-icon"><i class="fa fa-globe"></i></div>--}}
                                    {{--<h5 class="box-title">Logistics solutions</h5>--}}
                                {{--</div>--}}
                                {{--<div class="box-content">--}}
                                    {{--Smart and sustainable business requires the skills of logistics experts who are able to think ahead.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div><!-- /.item-three-column -->--}}
                    {{--</div><!-- /.flat-iconbox-style -->--}}
                {{--</div><!-- /.flat-wrapper -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<div class="flat-row parallax parallax4 pad-top120px pad-bottom120px">--}}
        {{--<div class="overlay bg-scheme1"></div>--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="make-quote">--}}
                        {{--<h1 class="title">From around the corner to<br>around the globe.</h1>--}}
                        {{--<h5 class="desc">We will take care of your cargo or your passenger and deliver them safe and on time.</h5>--}}
                        {{--<div class="group-btn">--}}
                            {{--<a class="button lg" href="#">make a quote <i class="fa fa-chevron-right"></i></a>--}}
                            {{--<a class="button lg outline style1" href="contact.html">contact us <i class="fa fa-chevron-right"></i></a>--}}
                        {{--</div>--}}
                    {{--</div><!-- /.make-quote -->--}}
                {{--</div><!-- /.col-md-12 -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<!-- Promobox -->--}}
    {{--<div class="flat-row bg-scheme1 pad-top0px pad-bottom0px">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<div class="promobox style1 style2 clearfix">--}}
                        {{--<h5 class="promobox-title">Contact us now to get quote for all your global shipping and cargo need.</h5>--}}
                        {{--<a class="button black sm" href="contact.html">contact us<i class="fa fa-chevron-right"></i></a>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-12 -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<div class="flat-row blog-shortcode blog-home pad-top60px pad-bottom0px">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-12">--}}
                    {{--<h2 class="flat-title-section style mag-bottom0px">Latest <span class="scheme">news.</span></h2>--}}
                {{--</div><!-- /.col-md-12 -->--}}
            {{--</div><!-- /.row -->--}}

            {{--<div class="row">--}}
                {{--<div class="content-wrap">--}}
                    {{--<div class="main-content">--}}
                        {{--<div class="main-content-wrap">--}}
                            {{--<div class="content-inner clearfix">--}}
                                {{--<article class="flat-item item-three-column blog-post">--}}
                                    {{--<div class="entry-wrapper">--}}
                                        {{--<div class="entry-cover">--}}
                                            {{--<a href="blog-single.html">--}}
                                                {{--<img src="images/blog/b1.jpg" alt="images">--}}
                                            {{--</a>--}}
                                        {{--</div><!-- /.entry-cover -->--}}
                                        {{--<div class="entry-header">--}}
                                            {{--<div class="entry-header-content">--}}
                                                {{--<h4 class="entry-time">--}}
                                                    {{--<span class="entry-day">25</span>--}}
                                                    {{--<span class="entry-month">Mar</span>--}}
                                                {{--</h4>--}}
                                                {{--<h4 class="entry-title">--}}
                                                    {{--<a href="blog-single.html">Raising productivity &amp; morale in the warehouse</a>--}}
                                                {{--</h4>--}}
                                            {{--</div><!-- /.entry-header-content -->--}}
                                        {{--</div><!-- /.entry-header -->--}}

                                        {{--<div class="entry-content">--}}
                                            {{--<p>It’s a well-known fact that happy and motivated workers produce better results. A recent study found that happier workers were 12% ...</p>--}}
                                        {{--</div><!-- /.entry-content -->--}}
                                        {{--<div class="entry-footer">--}}
                                            {{--<div class="entry-meta">--}}
                                                {{--<i class="fa fa-user"></i>--}}
                                                {{--<span class="entry-author"><a href="#">admin</a></span>--}}
                                                {{--<i class="fa fa-folder-open"></i>--}}
                                                {{--<span class="entry-categories"><a href="#">Warehouse</a></span>--}}
                                                {{--<i class="fa fa-comment"></i>--}}
                                                {{--<span class="entry-comments-link"><a href="#">0 Comment</a></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div><!-- /.entry-wrapper -->--}}
                                {{--</article><!-- /.blog-post -->--}}

                                {{--<article class="flat-item item-three-column blog-post">--}}
                                    {{--<div class="entry-wrapper">--}}
                                        {{--<div class="entry-cover">--}}
                                            {{--<a href="blog-single.html">--}}
                                                {{--<img src="images/blog/b2.jpg" alt="images">--}}
                                            {{--</a>--}}
                                        {{--</div><!-- /.entry-cover -->--}}
                                        {{--<div class="entry-header">--}}
                                            {{--<div class="entry-header-content">--}}
                                                {{--<h4 class="entry-time">--}}
                                                    {{--<span class="entry-day">25</span>--}}
                                                    {{--<span class="entry-month">Mar</span>--}}
                                                {{--</h4>--}}
                                                {{--<h4 class="entry-title">--}}
                                                    {{--<a href="blog-single.html">Seafield logistics goes into administration</a>--}}
                                                {{--</h4>--}}
                                            {{--</div><!-- /.entry-header-content -->--}}
                                        {{--</div><!-- /.entry-header -->--}}

                                        {{--<div class="entry-content">--}}
                                            {{--<p>Seafield Logistics has gone into administration and two of its operational units have been sold. David Riley, Les Ross and Joe ...</p>--}}
                                        {{--</div><!-- /.entry-content -->--}}
                                        {{--<div class="entry-footer">--}}
                                            {{--<div class="entry-meta">--}}
                                                {{--<i class="fa fa-user"></i>--}}
                                                {{--<span class="entry-author"><a href="#">admin</a></span>--}}
                                                {{--<i class="fa fa-folder-open"></i>--}}
                                                {{--<span class="entry-categories"><a href="#">Jobs</a></span>--}}
                                                {{--<i class="fa fa-comment"></i>--}}
                                                {{--<span class="entry-comments-link"><a href="#">0 Comment</a></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div><!-- /.entry-wrapper -->--}}
                                {{--</article><!-- /.blog-post -->--}}

                                {{--<article class="flat-item item-three-column blog-post">--}}
                                    {{--<div class="entry-wrapper">--}}
                                        {{--<div class="entry-cover">--}}
                                            {{--<a href="blog-single.html">--}}
                                                {{--<img src="images/blog/b3.jpg" alt="images">--}}
                                            {{--</a>--}}
                                        {{--</div><!-- /.entry-cover -->--}}
                                        {{--<div class="entry-header">--}}
                                            {{--<div class="entry-header-content">--}}
                                                {{--<h4 class="entry-time">--}}
                                                    {{--<span class="entry-day">25</span>--}}
                                                    {{--<span class="entry-month">Mar</span>--}}
                                                {{--</h4>--}}
                                                {{--<h4 class="entry-title">--}}
                                                    {{--<a href="blog-single.html">Transport managers grow scarce</a>--}}
                                                {{--</h4>--}}
                                            {{--</div><!-- /.entry-header-content -->--}}
                                        {{--</div><!-- /.entry-header -->--}}

                                        {{--<div class="entry-content">--}}
                                            {{--<p>Welcome to the Logistics Job Shop Newsletter; your need-to-know digest and analysis of the events of the past fortnight in the roa...</p>--}}
                                        {{--</div><!-- /.entry-content -->--}}
                                        {{--<div class="entry-footer">--}}
                                            {{--<div class="entry-meta">--}}
                                                {{--<i class="fa fa-user"></i>--}}
                                                {{--<span class="entry-author"><a href="#">admin</a></span>--}}
                                                {{--<i class="fa fa-folder-open"></i>--}}
                                                {{--<span class="entry-categories"><a href="#">Transport</a></span>--}}
                                                {{--<i class="fa fa-comment"></i>--}}
                                                {{--<span class="entry-comments-link"><a href="#">0 Comment</a></span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div><!-- /.entry-wrapper -->--}}
                                {{--</article><!-- /.blog-post -->--}}
                            {{--</div><!-- /.content-inner -->--}}
                        {{--</div><!-- /.main-content-wrap -->--}}
                    {{--</div><!-- /.main-content -->--}}
                {{--</div><!-- /.content-wrap  -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}



    {{--<!-- Promobox -->--}}
    {{--<div class="flat-row bg-scheme pad-top20px pad-bottom20px">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-2">--}}
                    {{--<div class="clients-image style">--}}
                        {{--<div class="item-img">--}}
                            {{--<img src="images/client/c1.png" alt="images">--}}
                        {{--</div>--}}
                        {{--<p class="tooltip">Laurentides</p>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-2 -->--}}

                {{--<div class="col-md-2">--}}
                    {{--<div class="clients-image style">--}}
                        {{--<div class="item-img">--}}
                            {{--<img src="images/client/c2.png" alt="images">--}}
                        {{--</div>--}}
                        {{--<p class="tooltip">Veolia</p>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-2 -->--}}

                {{--<div class="col-md-2">--}}
                    {{--<div class="clients-image style">--}}
                        {{--<div class="item-img">--}}
                            {{--<img src="images/client/c3.png" alt="images">--}}
                        {{--</div>--}}
                        {{--<p class="tooltip">Plane Business</p>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-2 -->--}}

                {{--<div class="col-md-2">--}}
                    {{--<div class="clients-image style">--}}
                        {{--<div class="item-img">--}}
                            {{--<img src="images/client/c4.png" alt="images">--}}
                        {{--</div>--}}
                        {{--<p class="tooltip">Arrow GLS</p>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-2 -->--}}

                {{--<div class="col-md-2">--}}
                    {{--<div class="clients-image style">--}}
                        {{--<div class="item-img">--}}
                            {{--<img src="images/client/c5.png" alt="images">--}}
                        {{--</div>--}}
                        {{--<p class="tooltip">MWR Transport</p>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-2 -->--}}

                {{--<div class="col-md-2">--}}
                    {{--<div class="clients-image style">--}}
                        {{--<div class="item-img">--}}
                            {{--<img src="images/client/c6.png" alt="images">--}}
                        {{--</div>--}}
                        {{--<p class="tooltip">Bradbell</p>--}}
                    {{--</div>--}}
                {{--</div><!-- /.col-md-2 -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<!-- Map -->--}}
    {{--<div class="flat-row pad-top0px pad-bottom0px">--}}
        {{--<div id="flat-map"></div>--}}
    {{--</div><!-- /.flat-row -->--}}

    {{--<div class="flat-row pad-top65px pad-bottom80px">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-md-8 col-md-offset-2">--}}
                    {{--<h2 class="flat-title-section title-center">Request a quick quote.</h2>--}}
                    {{--<p class="text-center">Fill out the form to get your quote within the hour. We guaranty safe and timley<br>product delivery either for your personal travel or your products.</p>--}}
                    {{--<div class="flat-divider d20px"></div>--}}
                    {{--<form id="contactform" method="post" action="./contact/contact-process.php">--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-md-6">--}}
                                {{--<p><input name="author" type="text" value="" placeholder="Location" required="required"></p>--}}

                                {{--<p><input id="email" name="email" type="email" value="" placeholder="To Destination" required="required"></p>--}}

                                {{--<p><select class="wpcf7-form-control wpcf7-select"><option value="Cargo">Cargo</option><option value="Person">Person</option></select></p>--}}

                                {{--<p><input id="phone" name="phone" type="text" value="" placeholder="Phone Number" required="required"></p>--}}
                            {{--</div><!-- /.col-md-6 -->--}}

                            {{--<div class="col-md-6">--}}
                                {{--<p><textarea name="comment" placeholder="Comment" required="required"></textarea></p>--}}
                                {{--<span class="form-submit"><input name="submit" type="submit" id="submit" class="submit" value="Get a quote">--}}
                                    {{--</span>--}}
                            {{--</div><!-- /.col-md-6 -->--}}
                        {{--</div><!-- /.row -->--}}
                    {{--</form>--}}
                {{--</div><!-- /.col-md-8 -->--}}
            {{--</div><!-- /.row -->--}}
        {{--</div><!-- /.container -->--}}
    {{--</div><!-- /.flat-row -->--}}

{{--@include('layouts.partials.footer')--}}

    {{--<!-- Go Top -->--}}
    {{--<a class="go-top">--}}
        {{--<i class="fa fa-chevron-up"></i>--}}
    {{--</a>--}}

{{--</div>--}}

{{--<!-- Javascript -->--}}
{{--<script type="text/javascript" src="javascript/jquery.min.js"></script>--}}
{{--<script type="text/javascript" src="javascript/bootstrap.min.js"></script>--}}
{{--<script type="text/javascript" src="javascript/jquery.easing.js"></script>--}}
{{--<script type="text/javascript" src="javascript/owl.carousel.js"></script>--}}
{{--<script type="text/javascript" src="javascript/jquery-waypoints.js"></script>--}}
{{--<script type="text/javascript" src="javascript/jquery-countTo.js"></script>--}}
{{--<script type="text/javascript" src="javascript/jquery.cookie.js"></script>--}}
{{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>--}}
{{--<script type="text/javascript" src="javascript/gmap3.min.js"></script>--}}
{{--<script type="text/javascript" src="javascript/jquery-validate.js"></script>--}}
{{--<script type="text/javascript" src="javascript/switcher.js"></script>--}}
{{--<script type="text/javascript" src="javascript/main.js"></script>--}}

{{--<!-- Revolution Slider -->--}}
{{--<script type="text/javascript" src="javascript/jquery.themepunch.tools.min.js"></script>--}}
{{--<script type="text/javascript" src="javascript/jquery.themepunch.revolution.min.js"></script>--}}
{{--<script type="text/javascript" src="javascript/slider.js"></script>--}}

{{--</body>--}}
{{--</html>--}}