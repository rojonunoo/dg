<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    //

    public function home(){
        return view('pages.home');
    }

//    Company  Pages here
    public function about_us(){
        return view('pages.company.about_us');
    }


    public function our_business(){
        return view('pages.company.our_business');
    }

    public function mission(){
        return view('pages.company.mission');
    }

    public function faq(){
        return view('pages.company.faq');
    }
//    End of company pages

    public function contact_us(){
        return view('pages.contact');
    }

}
